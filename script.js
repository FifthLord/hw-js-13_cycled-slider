
document.body.insertAdjacentHTML('beforeend', `<div class="timer"></div>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-stop">Припинити показ</button>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-start" disabled>Відновити показ</button>`);

btnStop = document.querySelector('.btn-stop');
btnStart = document.querySelector('.btn-start');

btnStart.addEventListener('click', e => {
   if (e.target.closest('.btn-start')) {
      changeImg(count);
   }
});
btnStop.addEventListener('click', e => {
   if (e.target.closest('.btn-stop')) {
      console.log("stop");
      timer.innerHTML = 2 + ' : ' + 999;
      clearTimeout(imgTimerId);
      clearInterval(timerId);
      btnStop.setAttribute("disabled", '');
      btnStart.removeAttribute("disabled")
   }
});

//---Основна логіка зміни зображення---//
const images = document.querySelectorAll('.image-to-show');
const timer = document.querySelector('.timer');
let count = 0;
let imgTimerId;
let timerId;

function changeImg() {
   if (count > images.length - 1) count = 0;
   startTime()
   images[count].classList.add('active');
   imgTimerId = setTimeout(() => {
      images[count].classList.add('fade-out');
      setTimeout(() => {
         images[count].classList.remove('active', 'fade-out');
         count++;
         clearInterval(timerId);
         changeImg()
      }, 500);
   }, 2500);
};
changeImg()

//---логіка таймеру---//
function startTime() {
   console.log("tick");
   let responseTime = new Date(Date.now() + (1000 * 3));
   let countdown = new Date();
   timerId = setInterval(function () {
      countdown.setTime(responseTime - Date.now());
      timer.innerHTML = (countdown.getUTCSeconds()) + ' : ' + countdown.getUTCMilliseconds();
   }, 10);

   btnStart.setAttribute("disabled", '');
   btnStop.removeAttribute("disabled")
};